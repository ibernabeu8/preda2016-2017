package es.uned.nreinas;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
public class InputOutput {

	public static String[] LeerFichero(String ruta) throws FileNotFoundException, IOException{
		String linea;
		String[] aNumeros = new String[2];
		FileReader fr = new FileReader(ruta);
		BufferedReader reader = new BufferedReader(fr);
		
		while((linea = reader.readLine()) != null) {
			aNumeros[0] = linea;
		}
		
		reader.close();
		fr.close();
		return aNumeros;
	}
	
	public static void escribeFichero(String ruta, StringBuilder traza) throws FileNotFoundException, IOException{
		FileWriter fw = new FileWriter(ruta);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(traza.toString());
		bw.close();
		fw.close();
	}
}

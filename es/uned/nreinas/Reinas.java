package es.uned.nreinas;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Reinas {
	
	private static int numSolucion = 0;
	private static StringBuilder traza = new StringBuilder();
	private static StringBuilder trazaTablero = new StringBuilder();
	
	public static void calculaReinas(int numReinas,boolean grafico) {
		int[] tablero = new int[numReinas];
		reinas(tablero,0,grafico);
	}
	
	private static void reinas(int[] vector, int reina, boolean grafico) {
		int max = vector.length;
		
		if( reina == max && grafico) 
			pintarTablero(vector);
		else if( reina == max && !grafico)
			pintarTraza(vector);
		else {
			for(int i = 0; i < max; i++) {
				vector[reina] = i;
				if(completable(vector,reina))
					reinas(vector,reina+1,grafico);
			}
		}
	}
	
	private static boolean completable(int[] vector, int reina) {
		
		for(int i = 0; i < reina; i++) {
			if(vector[i] == vector[reina])
				return false; //misma columna
			if((vector[i] - vector[reina]) == (reina - i))
				return false; //misma diagonal mayor
			if((vector[reina] - vector[i]) == (reina - i))
				return false; //misma diagonal menor
		}
		return true;
	}
	/*
	private static void pintarTraza(int[] tablero) {
		int n = tablero.length;
		System.out.println("\nSolucion: " + ++numSolucion);
		
		for(int i = 0; i < n; i++) {
			for(char columna = 'A'; columna < n; columna++) {
				if(tablero[i] == columna)
					System.out.print(i + columna);
			}
			System.out.print(" ");
		}
		System.out.println();
	}*/
	private static void pintarTraza(int[] tablero) {
		/*int n = tablero.length;
		traza.append("\nSolucion: " + ++numSolucion + "\n");
		
		for(int i = 0; i < n; i++) {
			char columna = 'A';
			for(int columnaNum = 0; columna < n; columna++) {		
				if(tablero[i] == columnaNum) {
					traza.append(i + columna);
				}
				columna++;	
			}
			traza.append(" ");
		}
		traza.append("\n");
		*/
		
		
		int n = tablero.length;
		traza.append("\nSolucion: " + ++numSolucion + "\n");
        for (int i = 0; i < n; i++) {
        	
        	//Rellena las filas con reinas o espacios
            for (int j = 0; j < n; j++) {
                if (tablero[i] == j)
                	traza.append("C" + j + "F" + i + " ");
            }   
        }  
        traza.append("\n");
    	for(int anchura = 0; anchura < n; anchura++) {
    		traza.append("----");
    	}
    	traza.append("\n");
	}
	/*
	private static void pintarTablero(int[] tablero) {
		int n = tablero.length;
		System.out.println("\nSolucion: " + ++numSolucion);
        for (int i = 0; i < n; i++) {
        	//Pinta las lineas de separacion de cada casilla
        	for(int anchura = 0; anchura < n; anchura++) {
        		System.out.print("----");
        	}
        	System.out.println();
        	//Rellena las filas con reinas o espacios
            for (int j = 0; j < n; j++) {
                if (tablero[i] == j)
                	System.out.print(" Q |");
                else
                	System.out.print(" * |");
            }
            System.out.println("");
        }  
       
    	for(int anchura = 0; anchura < n; anchura++) {
    		System.out.print("----");
    	}
    	System.out.println();
	}
	*/
	
	private static void pintarTablero(int[] tablero) {
		int n = tablero.length;
		trazaTablero.append("\nSolucion: " + ++numSolucion + "\n");
        for (int i = 0; i < n; i++) {
        	//Pinta las lineas de separacion de cada casilla
        	for(int anchura = 0; anchura < n; anchura++) {
        		trazaTablero.append("----");
        	}
        	trazaTablero.append("\n");
        	//Rellena las filas con reinas o espacios
            for (int j = 0; j < n; j++) {
                if (tablero[i] == j)
                	trazaTablero.append(" Q |");
                else
                	trazaTablero.append(" * |");
            }
            trazaTablero.append("\n");
        }  
       
    	for(int anchura = 0; anchura < n; anchura++) {
    		trazaTablero.append("----");
    	}
    	trazaTablero.append("\n");
	}
	public static void ayuda() {
		System.out.println("SINTAXIS:");
		System.out.println("reinas [-t][-h] N [fichero_salida]");
		System.out.println("-t %-30s%s Traza");
		System.out.println("-g %-30s%s Modo grafico");
		System.out.println("-h %-30s%s Muestra ayuda");
		System.out.println("N %-30s%s Tamaño del tablero y numero de reinas.");
		System.out.println("fichero_salida %-30s% Nombre del fichero de salida");
	}
	public static void main(String[] args) throws FileNotFoundException, IOException {
		int argsc = args.length;
		//String parametro, numReinas, rutaSalida;
		
		//System.out.println(trazaTablero);
		//InputOutput.escribeFichero(ruta, trazaTablero);
		//2 argumentos son opcionales, el orden es reinas [-t][-h] N [fichero_salida]
		switch(argsc) {
		//Se pasan unicamente el tamaño del tablero o la ayuda -h
		case 1:
			if(args[0].toLowerCase().equals("-h")) {
				ayuda();
			}else{
				calculaReinas(Integer.valueOf(args[0]),false);
				System.out.println(traza);
			}
			break;
		//Se pasa fichero salida o traza y tamaño del tablero
		case 2:
			if(args[0].toLowerCase().equals("-t")) {
				calculaReinas(Integer.valueOf(args[1]),false);
				System.out.println(traza);
			}
			else if(args[0].toLowerCase().equals("-g")) {
				calculaReinas(Integer.valueOf(args[1]),true);
				System.out.println(trazaTablero);
			}
			else if(args[0].toLowerCase().equals("-h")) {
				ayuda();
			}
			
			break;
		//Se pasan 3 parametros, si el primero es H imprime ayuda
		case 3:
			if(args[0].toLowerCase().equals("-h")) {
				ayuda();
			} else if(args[0].toLowerCase().equals("-g")) {
				calculaReinas(8, true);
				InputOutput.escribeFichero(args[2], trazaTablero);
			} else if(args[0].toLowerCase().equals("-t")) {
				calculaReinas(8, false);
				InputOutput.escribeFichero(args[2], traza);
			} else {
				System.out.println("Hay un parametro erroneo.");
			}	
			break;
		default:
			System.out.println("Numero de argumentos incorrecto.");
			break;
		}
	}
}

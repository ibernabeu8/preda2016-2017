package ped1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Scanner;

public class AlgoritmoMult {

	static StringBuilder traza = new StringBuilder();
	
	static BigInteger multiplicacionKaratsuba(BigInteger n1, BigInteger n2) {
		
		//Número de cifras de los operando, tomamos el que 
		int longitudNumero = Math.max(n1.bitLength(), n2.bitLength());
		
		traza.append(n1.bitLength() + " este es el 1, y este el 2 : " +n2.bitLength() + "\n");
		
		//TRAZA
		traza.append("\nNumero uno: " + n1);
		traza.append("\nNumero dos: " + n2 + "\n");
		traza.append("\nLongitud máxima entre los dos números: " + longitudNumero + "\n");
		//END TRAZA
		
		//Si la longitud es 1 entonces multiplicamos directamente
		if(longitudNumero <= 5)
			return n1.multiply(n2); //solucion trivial
		
		/**********DESCOMPONER************/
		//Partimos el numero en 2
		int numeroPosiciones = longitudNumero/2;
		traza.append("\nNúmero de posiciones a mover: " + numeroPosiciones + "\n");
		
		//Numero X lado izquierdo x1, lado derecho x2 
		BigInteger x2 = n1.shiftRight(numeroPosiciones);
		BigInteger x1 = n1.subtract(x2.shiftLeft(numeroPosiciones));
		//Traza
		traza.append("\nLado izquierdo primer numero: " + x1 + "\n");
		traza.append("Lado derecho primer numero: " + x2 + "\n");
		//
		//numero Y lado izquierdo y1, lado derecho y2 
		BigInteger y2 = n2.shiftRight(numeroPosiciones);
		BigInteger y1 = n2.subtract(y2.shiftLeft(numeroPosiciones));		
		//Traza
		traza.append("\nLado izquierdo segundo numero: " + y1 + "\n");
		traza.append("Lado derecho segundo numero: " + y2 + "\n");
		//
		/**********FIN DESCOMPONER******/
		
		//Aplicamos karatsuba recursivamente para resolver las soluciones parciales
		BigInteger z0 = multiplicacionKaratsuba(x1, y1);
		BigInteger z1 = multiplicacionKaratsuba(x1.add(x2),y1.add(y2));
		BigInteger z2 = multiplicacionKaratsuba(x2, y2);
		BigInteger resta = z1.subtract(z2).subtract(z0);
		
		traza.append("\nZ0 = " + z0);
		traza.append("\nZ1 = " + z1);
		traza.append("\nZ2 = " + z2 + "\n");
		//Combinamos las soluciones parciales
		BigInteger solucion = z0.add(resta.shiftLeft(numeroPosiciones)).add(z2.shiftLeft(2*numeroPosiciones));
		traza.append("Solucion: " + solucion +"\n\n");
		return solucion;
	}
	
	public static void traza() {
		System.out.println(traza);
	}
	public static void traza(String ruta) throws FileNotFoundException, IOException {
		InputOutput.escribeFichero(ruta, traza);
	}
	
	public static BigInteger[] leeConsola() {
		BigInteger[] numeros = new BigInteger[2];
		Scanner sc = new Scanner(System.in);
		System.out.print("Introduce el primer número: ");
		numeros[0] = sc.nextBigInteger();
		System.out.print("Introduce el segundo número: ");
		numeros[1] = sc.nextBigInteger();
		sc.close();
		return numeros;
	} 
	public static void ayuda() {
			System.out.println("SINTAXIS:");
			System.out.println("multiplica [-t][-h] [fichero_entrada] [fichero_salida]");
			System.out.println("-t %-30s% Traza");
			System.out.println("-h %-30s% Muestra ayuda");
			System.out.println("fichero_salida %-30s% Nombre del fichero de salida");
	}
	public static void main(String[] args) throws FileNotFoundException, IOException {

		int argsc = args.length;
		String parametro, ficheroEntrada, ficheroSalida;
		
		if(argsc == 3) {
			parametro = args[0];
			ficheroEntrada = args[1];
			ficheroSalida = args[2];
			
			switch(parametro.toLowerCase()) {
			
			case "-h":
				ayuda();
			break;
			case "-t":
				String[] numeros = InputOutput.LeerFichero(ficheroEntrada);
				BigInteger n1 = new BigInteger(numeros[0]);
				BigInteger n2 = new BigInteger(numeros[1]);
				multiplicacionKaratsuba(n1, n2);
				InputOutput.escribeFichero(ficheroSalida, traza);
				break;
			default:
				break;
			
			}
		}else if(argsc == 2) {
			ficheroEntrada = args[1];
			parametro = args[1];
			if(parametro.toLowerCase().equals("-h")) {
				ayuda();
			}
			else
			{
				String[] numeros = InputOutput.LeerFichero(ficheroEntrada);
				BigInteger n1 = new BigInteger(numeros[0]);
				BigInteger n2 = new BigInteger(numeros[1]);
				multiplicacionKaratsuba(n1, n2);
				traza();
			}

		}else if(argsc == 0){
			BigInteger[] numeros = new BigInteger[2];
			numeros = leeConsola();
			System.out.println(multiplicacionKaratsuba(numeros[0], numeros[1]));
		}else {//lee de consola
			parametro = args[0];
			if(parametro.toLowerCase().equals("-h")) {
				ayuda();
			}else if(parametro.toLowerCase().equals("-t")) {
				BigInteger[] numeros = new BigInteger[2];
				numeros = leeConsola();
				multiplicacionKaratsuba(numeros[0], numeros[1]);
				traza();				
			}else {
				BigInteger[] numeros = new BigInteger[2];
				numeros = leeConsola();
				System.out.println(multiplicacionKaratsuba(numeros[0], numeros[1]));
			}

		}
		
	}
	
}

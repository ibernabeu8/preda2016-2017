package ped1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
public class InputOutput {

	public static String[] LeerFichero(String ruta) throws FileNotFoundException, IOException{
		String linea;
		String[] aNumeros = new String[2];
		FileReader fr = new FileReader(ruta);
		BufferedReader reader = new BufferedReader(fr);
		System.out.println("pre");
		int i = 0;
		while((linea = reader.readLine()) != null) {
			if(linea.trim().length() > 0) {//para comprobar que tiene algo mas q espacios
				aNumeros[i] = linea;
				//System.out.println(aNumeros[i]);
				i++;
			}
		}
		reader.close();
		fr.close();
		return aNumeros;
	}
	
	public static void escribeFichero(String ruta, StringBuilder traza) throws FileNotFoundException, IOException{
		
		FileWriter fw = new FileWriter(ruta);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(traza.toString());
		bw.close();
		fw.close();
	}
}
